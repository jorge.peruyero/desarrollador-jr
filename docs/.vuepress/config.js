module.exports = {
  title: "Curso para desarrollador jr Digiceo",
  description: "Curso Digiceo",
  base: "/desarrollador-jr/",
  dest: "public",
  plugins: ["@vuepress/back-to-top", "@vuepress/medium-zoom"],
  themeConfig: {
    logo: "/assets/img/logo.png",
    smoothScroll: true,
    nav: [{ text: "Inicio", link: "/" }],
    sidebar: {
      "/empezar/": ["", "instalacionYConfiguracion"],

      "/git/": ["", "configuracion"],

      "/html/": ["", "etiquetasBasicas", "semantica", "multimedia", "formularios"],
      
      "/css/": ["", "bases", "boxmodel", "position"],
    },
  },
};
