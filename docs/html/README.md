# HTML

## Qué es HTML

HTML (Lenguaje de marcado de hipertexto) es el más básico componente de la Web. Define el significado y la estructura del contenido web. Además de HTML, se utilizan otras tecnologías generalmente para describir la apariencia/presentación de una página web (CSS) o la funcionalidad/comportamiento (JavaScript.

"Hipertexto" se refiere a enlaces que conectan páginas web entre sí, ya sea dentro de un único sitio web o entre sitios web. Los enlaces son un aspecto fundamental de la Web. Al cargar contenido en Internet y vincularlo a páginas creadas por otras personas, te conviertes en un participante activo en la «World Wide Web».

HTML utiliza "marcado" para etiquetar texto, imágenes y otro contenido para mostrarlo en un navegador web. El marcado HTML incluye "elementos" especiales como `<head>`, `<title>`, `<body>`, `<header>`, `<footer>`, `<article>`, `<section>`, `<p>`, `<div>`, `<span>`, `<img>`, `<aside>`, `<audio>`, `<canvas>`, `<datalist>`, `<details>`, `<embed>`, `<nav>`, `<output>`, `<progress>`, `<video>`, `<ul>`, `<ol>`, `<li>` y muchos otros.

Un elemento HTML se distingue de otro texto en un documento mediante "etiquetas", que consisten en el nombre del elemento rodeadas por "<" y ">". El nombre de un elemento dentro de una etiqueta no distingue entre mayúsculas y minúsculas. Es decir, se puede escribir en mayúsculas, minúsculas o una mezcla. Por ejemplo, la etiqueta `<title>` se puede escribir como `<Title>`, `<TITLE>` o de cualquier otra forma.

## Estructura de las etiquetas HTML

  ``` HTML
    <div id="divPrincipal" class="clase1"> Contenido </div>
  ```

  ``` HTML
    <img id="imgPortada" class="claseImgPortada" src="img1.jpg">
  ```

## Estructura básica de un documento HTML

``` HTML
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  
</body>
</html>
```
