# Semántica

En programación, la semántica se refiere al significado de un fragmento de código; por ejemplo, "¿qué efecto tiene ejecutar esa línea de JavaScript?", O "qué propósito o función tiene ese elemento HTML" (en lugar de "¿cómo se ve? ".)

## Encabzado

El elemento de HTML Header `<header>` representa un grupo de ayudas introductorias o de navegación. Puede contener algunos elementos de encabezado, así como también un logo, un formulario de búsqueda, un nombre de autor y otros componentes.

## Pie de página

El Elemento HTML Footer `<footer>` representa un pie de página para el contenido de sección más cercano o el elemento  raíz de sección.

Un pie de página típicamente contiene información acerca de el autor de la sección, datos de derechos de autor o enlaces a documentos relacionados.

## Secciones

### `div`

div de "division" -división . Sirve para crear secciones o agrupar contenidos.

Sus etiquetas son: `<div>` y `</div>` (ambas obligatorias).

### `section`

El elemento de HTML section `<section>` representa una sección genérica de un documento. Sirve para determinar qué contenido corresponde a qué parte de un esquema. Piensa en el esquema como en el índice de contenido de un libro; un tema común y subsecciones relacionadas.  Es, por lo tanto, una etiquéta semántica. Su funcionalidad principal es estructurar semánticamente un documento a la hora de ser representado por parte de un agente usuario. Por ejemplo, un agente de usuario que represente el documento en voz, podría exponer al usuario el índice de contenido por niveles para navegar rápidamente por las distintas partes.

### `article`

El Elemento article de HTML `<article>` representa una composición auto-contenida en un documento, página, una aplicación o en el sitio, que se destina a distribuir de forma independiente o reutilizable, por ejemplo, en la indicación. Podría ser un mensaje en un foro, un artículo de una revista o un periódico, una entrada de blog, un comentario de un usuario, un widget interactivo o gadget, o cualquier otro elemento independiente del contenido.

### `aside`

El elemento HTML `<aside>` representa una sección de una página que consiste en contenido que está indirectamente relacionado con el contenido principal del documento. Estas secciones son a menudo representadas como barras laterales o como inserciones y contienen una explicación al margen como una definición de glosario, elementos relacionados indirectamente, como publicidad, la biografía del autor, o en aplicaciones web, la información de perfil o enlaces a blogs relacionados.

## Navegación

### `nav`

El elemento HTML `<nav>` representa una sección de una página cuyo propósito es proporcionar enlaces de navegación, ya sea dentro del documento actual o a otros documentos. Ejemplos comunes de secciones de navegación son menús, tablas de contenido e índices.

### `a`

El Elemento HTML Anchor `<a>` crea un enlace a otras páginas de internet, archivos o ubicaciones dentro de la misma página, direcciones de correo, o cualquier otra URL

``` HTML
<nav class="menu">
    <a href="#">Inicio</a>
    <a href="#">Sobre nosotros</a>
    <a href="#">Contacto</a>
</nav>
```
