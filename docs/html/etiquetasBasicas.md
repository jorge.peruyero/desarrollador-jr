# Etiquetas Básicas

## Head

La cabecera (en inglés Head) es la parte de un documento HTML que contiene metadatos sobre ese documento, como el autor, la descripción y los enlaces a los archivos CSS o JavaScript que se deben aplicar al documento HTML. Es la parte del documento web que no es visible al usuario.

## Body

El elemento `<body>` de HTML representa el contenido de un documento HTML. Solo puede haber un elemento `<body>` en un documento.

## Encabezados

Los elementos de encabezado implementan seis niveles de encabezado del documento, `<h1>` es el más importante, y `<h6>`, el menos importante. Un elemento de encabezado describe brevemente el tema de la sección que presenta. La información de encabezado puede ser usada por los agentes usuarios, por ejemplo, para construir una tabla de contenidos para un documento automáticamente

`h1 h2 h3 h4 h5 h6`

``` HTML
<h1>Encabezado nivel 1</h1>
<h2>Encabezado nivel 2</h2>
<h3>Encabezado nivel 3</h3>
<h4>Encabezado nivel 4</h4>
<h5>Encabezado nivel 5</h5>
<h6>Encabezado nivel 6</h6>
```

## Título

La etiqueta `title` sirve para asignar un nombre al documento el cual se verá reflejado en la pestaña del navegador

``` HTML
<title>Título de documento</title>
```

Este se coloca en el encabezado de nuestro sitio web

``` HTML
<head>
  <title>Document</title>
</head>
```

## Comentarios

- `<!--comentario -->`

## Párrafos

El elemento p (párrafo) es el apropiado para distribuir el texto en párrafos.

Sus etiquetas son: `<p>` y `</p>`

``` HTML
<p>
Esto
es un
párrafo
</p>
```

## Saltos de línea

El elemento HTML line break `<br>` produce un salto de línea en el texto (retorno de carro). Es útil para escribir un poema o una dirección, donde la división de las líneas es significante.

No utilices `<br>` para incrementar el espacio entre líneas de texto; para ello utiliza la propiedad margin de CSS o el elemento `<p>`.

Sus etiquetas son: `<br/>` (solo tiene una).

``` HTML
Mozilla Foundation<br>
1981 Landings Drive<br>
Building K<br>
Mountain View, CA 94043-0801<br>
USA
```

## Saltos de tema

El elemento HTML `<hr>` representa un cambio de tema entre párrafos (por ejemplo, un cambio de escena en una historia, un cambio de tema en una sección). En versiones previas de HTML representaba una línea horizontal. Aún puede ser representada como una línea horizontal en los navegadores visuales, pero ahora es definida en términos semánticos y no tanto en términos representativos, por tanto para dibujar una línea horizontal se debería usar el CSS apropiado.

Sus etiquetas son: `<hr/>` (solo tiene una).

## Formato de texto

`<i>`, `<small>`, `<strong>`

## Listas

### `ul`

`ul` de "unordered list" -lista no ordenada . crea una lista no ordenada.

Sus etiquetas son: `<ul>` y `</ul>` (ambas obligatorias).

### `ol`

El elemento `ol` permite definir listas o viñetas ordenadas (“Ordered List”), bien con numeración o alfabéticamente.

Sus etiquetas son: `<ol>` y `</ol>` (ambas obligatorias).

### `li`

El elemento `li` del ingles item list o elemento de lista declara cada uno de los elementos de una lista.

Sus etiquetas son: `<li>` y `</li>` (la de cierre es opcional).

### Lista ordenada

``` HTML
<ol>
  <li>1</li>
  <li>2</li>
  <li>3</li>
</ol>
```

### Lista **No** ordenada

``` HTML
<ul>
  <li>1</li>
  <li>2</li>
  <li>3</li>
</ul>
```

## Tablas

El Elemento de Tabla HTML `<table>` representa datos en dos o mas dimensiones.

Los elementos `<tr>` corresponde a una nueva fila mientras que para las columnas se tiene los elementos `<th>` que sirven para encabezados ya que estos se colocan en negritas y `<td>` para el resto del contenido de la tabla.

``` HTML
<table>
  <thead>
    <tr>
      <th>Header content 1</th>
      <th>Header content 2</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Body content 1</td>
      <td>Body content 2</td>
    </tr>
  </tbody>
</table>
```
