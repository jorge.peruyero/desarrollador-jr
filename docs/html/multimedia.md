# Multimedia

## Imágenes

### `img`

El elemento de imagen HTML `<img>` representa una imagen en el documento.

### `audio`

El elemento audio se usa para insertar contenido de audio en un documento HTML o XHTML. El elemento audio se agregó como parte de HTML 5.

``` HTML
<audio controls src=""></audio>
```

### `video`

El elemento video se usa para insertar contenido de video en un documento HTML o XHTML. El elemento video se agregó como parte de HTML 5.

``` HTML
<video controls src="" width="400"></video>
```
