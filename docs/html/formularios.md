# Formularios

El elemento HTML form `<form>` representa una sección de un documento que contiene controles interactivos que permiten a un usuario enviar información a un servidor web.

Es posible usar las pseudo-clasess de CSS :valid e :invalid  para darle estilos a un elemento form.

## `input`

El elemento HTML `<input>` se usa para crear controles interactivos para formularios basados en la web con el fin de recibir datos del usuario.Hay disponible una amplia variedad de tipos de datos de entrada y widgets de control, que dependen del dispositivo y el agente de usuario (user agent).El elemento `<input>` es uno de los más potentes y complejos en todo HTML debido a la gran cantidad de combinaciones de tipos y atributos de entrada.

## `label`

El Elemento HTML `<label>` representa una etiqueta para un elemento en una interfaz de usuario. Este puede estar asociado con un control ya sea mediante la utilizacion del atributo for, o ubicando el control dentro del elemento label. Tal control es llamado "el control etiquetado" del elemento label.

## `select`

El elemento select `<select>` de HTML representa un control que muestra un menú de opciones. Las opciones contenidas en el menú son representadas por elementos `<option>`, los cuales pueden ser agrupados por elementos `<optgroup>`. La opcion puede estar preseleccionada por el usuario.

``` HTML
<select name="select">
  <option value="value1">Value 1</option>
  <option value="value2" selected>Value 2</option>
  <option value="value3">Value 3</option>
</select>
```

## `button`

La etiqueta de HTML `<button>` representa un elemento cliqueable de tipo botón que puede ser utilizado en formularios o en cualquier parte de la página que necesite un botón estándar y simple de aplicar. De forma predeterminada, los botones HTML se presentan con un estilo similar en todas las plataformas, estos estilos se pueden cambiar utilizando CSS.

Sus etiquetas son: `<button>` y `</button>` (ambas obligatorias).
