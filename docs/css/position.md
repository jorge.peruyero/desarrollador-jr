# Position

## Conceptos básicos

Position es la propiedad que nos permite modificar el flujo del HTML

Los valores de position son:

- Static (Valor por defecto)
- Relative
- Absolute
- Fixed
- Sticky

Al tener un elemento posicionado podemos moverlo en los 3 ejes

- top: Podemos mover el elemento en relación a la parte superior.
- right: Podemos mover el elemento en relación a la parte derecha.
- bottom: Podemos mover el elemento en relación a la parte inferior.
- left: Podemos mover el elemento en relación a la parte izquierda.
- z-index: Podemos mover el elemento en el eje Z.

``` HTML

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Position - Conceptos básicos</title>
    <link rel="stylesheet" href="normalize.css">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
    <h1 class="title">Atributo position</h1>
    <p class="parrafo-1">Este es el primer párrafo</p>
    <p class="parrafo-2">Este es el segundo párrafo</p>
</body>
</html>
```

``` CSS
/*
Position
Flujo HTML -> Es el orden en el que aparecen los elementos en el documento
Espacio -> Es el espacio que ocupa un elemento en el documento
*/

.title {
    text-align: center;
    font-size: 3em;
}

p {
    font-size: 2em;
}

.parrafo-1{
    display: none;
}
```
