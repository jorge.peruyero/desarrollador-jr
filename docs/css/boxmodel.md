# Box model

## Introducción al Box model

En la web todo son cajas, al conjunto de esas cajas y la forma en la que el navegador los dibuja se le denomina layout.

Las propiedades principales de cada una de estas cajas son el ancho (width) y el alto (height).

Existen dos tipos de elementos en HTML, elementos en línea (inline) y de bloque (block) estas propiedades se pueden modificar con el atributo display.

- Elementos inline
  - Son elementos que soólo ocupan su contenido.
  - No se puede modificar su ancho ni su alto.
- Elementos block
  - Ocupan todo el ancho disponible
  - Se les puede asignar ancho y alto

![inline-y-cajas](/desarrollador-jr/assets/img/css/boxmodel/inline-y-cajas.png)

![estructura-de-cajas](/desarrollador-jr/assets/img/css/boxmodel/estructura-de-cajas.png)

### Ejemplo Box model

``` HTML
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Box Model</title>
  <link rel="stylesheet" href="styles.css">
</head>
<body>

<!-- <h1>Box Model</h1>
<h2>Modelo de caja</h2>
<a href="#">Enlace 1</a>
<a href="#">Enlace 2</a> -->

<div class="box-1">Contenido 1</div>
<div class="box-2">Contenido 2</div>

</body>
</html>
```

``` CSS
h1{
    background: lightblue;
    height: 150px;
    width: 100px;
}

h2{
    background: purple;
    width: 50px;
}

a{
    background: lime;
    width: 300px;
    height: 300px;
}

.box-1{
    width: 100px;
    height: 100px;
    background: lime;
    padding: 10px;
    border: 10px solid blue; /* sus dimensiones cuentan para el tamaño total */
    margin: 20px;
    outline: 10px solid red; /* sus dimensiones NO cuentan para el tamaño total */
}

.box-2{
    width: 100px;
    height: 100px;
    background: lightcoral;
}
```

## Margin y Padding

### Margin

Margin es la separación entre una caja y las cajas adyacentes.

Margin es un shorthand

- margin-top
- margin-right
- margin-bottom
- margin-left

Orden de Margin

- Margin: 1em; (top right bottom left)
- Margin: 1em 2em; (Y X)
- Margin: 1em 2em 3em; (top X bottom)
- Margin: 1em 2em 3em 4em; (top right bottom left)

### Padding

Padding es la separación entre el contenido y su borde.

Padding es un shorthand

- padding-top
- padding-right
- padding-bottom
- padding-left

Orden de Padding

- Padding: 1em; (top right bottom left)
- Padding: 1em 2em; (Y X)
- Padding: 1em 2em 3em; (top X bottom)
- Padding: 1em 2em 3em 4em; (top right bottom left)

### Ejemplo Margin y Padding

``` HTML
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Margin - padding</title>
  <link rel="stylesheet" href="normalize.css">
  <link rel="stylesheet" href="styles.css">
</head>
<body>
  <div class="box1"></div>
  <div class="box2">
    <div class="box2-content"></div>
  </div>
</body>
</html>
```

``` CSS
/*
Margin: 1em; (top right bottom left)
Margin: 1em 2em; (Y X)
Margin: 1em 2em 3em; (top, X, bottom)
Margin: 1em 2em 3em 4em; (top right bottom left)
Podemos aplicar las propiedades por separado
Margin-top: 1em;
Margin-rigt: 2em;
Margin-bottom: 3em;
Margin-left: 4em;
*/


.box1{
    width: 200px;
    height: 200px;
    background: lightgreen;
    margin: 1em 2em 3em 4em;
}

/*
Padding :  1em; (top right bottom left)
Padding :  1em 2em; (Y X)
Padding : 1em 2em 3em; (top, X, bottom)
Padding : 1em 2em 3em 4em; (top right bottom left)
Podemos aplicar las propiedades por separado
Padding-top: 1em;
Padding-rigt: 2em;
Padding-bottom: 3em;
Padding-left: 4em;
*/

.box2{
    width: 200px;
    height: 200px;
    background: lightcoral;
    border: 2px solid blue;
    padding: 1em 2em 3em 4em;
}

.box2-content{
    width: 20px;
    height: 20px;
    background: yellow;
    border: 2px solid red;
}
```

## Border

Border es la línea que rodea la caja

Border es un shorthand

Las propiedades de los bordes son:

- Width
- Style
- Color

Existen bastantes combinaciones

- border-width: 2px; (top right bottom left)
- border-style: solid dotted; (Y X)
- border-color: red blue green; (top X bottom)
- border-width: 2px 3px 4px 5px; (top right bottom left)

### Ejemplo Border

``` HTML
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Border</title>
  <link rel="stylesheet" href="normalize.css">
  <link rel="stylesheet" href="styles.css">
</head>
<body>
  <div class="box1"></div>
  <div class="box2"></div>
  <div class="box3"></div>
  <div class="box4"></div>
  <div class="box5"></div>
  <div class="box6"></div>
  <div class="box7"></div>
  <div class="box8"></div>
  <div class="box9"></div>
  <div class="box10"></div>
</body>
</html>
```

``` CSS
/* Border
border-width
    px|em|rem|%|....
border-style
    none|hidden|dotted|dashed|solid|double|groove|ridge|inset|outset|initial|inherit;
    *Estas medidas dependen del navegador*
    thick (ancho)
    medium (medio)
    thin (fino)

border-color
    css native|rgb|rgba|hsl|hsla|hexadecimal|transparent
border-top-(width | style | color)
border-right-(width | style | color)
border-bottom-(width | style | color)
border-left-(width | style | color)
*/

[class*=box] {
    width: 200px;
    height: 200px;
    margin: 2em;
}

.box1 {
    background: yellowgreen;
    border: 5px solid red;
    border-top-width: 200px;
    border-top-color: transparent;
}

.box2 {
    background: lightseagreen;
    border-left-width: 5px;
    border-left-style: dotted;
    border-left-color: blue;
}

.box3 {
    background: lightcoral;
    border: 5px dashed blue;
}

.box4 {
    background: yellowgreen;
    border: 5px double blue;
}

.box5 {
    background: lightseagreen;
    border: 5px groove blue;
}

.box6 {
    background: lightcoral;
    border: 5px ridge blue;
}

.box7 {
    background: yellowgreen;
    border: 5px inset blue;
}

.box8 {
    background: lightseagreen;
    border: 5px outset blue;
}

/* initial es la propiedad que tuviera por defecto, en este caso es none */

.box9 {
    background: lightcoral;
    border: 5px initial blue;
}

/* inherit es la propiedad heredada, si el padre tuviera border pues cogería ese estilo */

.box10 {
    background: yellowgreen;
    border: 5px inherit blue;
}
```

## Outline

Outline es la línea que rodea la caja entre el border y el margin

Outline es un shorthand

Laspropiedades de outline son:

- Width
- Style
- Color
- Offset

Existen bastantes combinaciones

- outline-width: 2px; (top right bottom left)
- outline-style: solid dotted; (Y X)
- outline-color: red blue green; (top X bottom)
- outline-width: 2px 3px 4px 5px; (top right bottom left)
- outline-offset: +-px|em|rem...

### Ejemplo Outline

![gamepad](/desarrollador-jr/assets/img/css/boxmodel/gamepad.jpeg)

``` HTML
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Ouline</title>
  <link rel="stylesheet" href="normalize.css">
  <link rel="stylesheet" href="styles.css">
</head>
<body>
  <div class="img-box">
    <img class="img" src="gamepad.jpeg" alt="">
    <p class="text">GAMEPAD</p>
  </div>
</body>
</html>
```

``` CSS
/*
Outline
outline-width
    px|em|rem|%|....
outline-style
    none|hidden|dotted|dashed|solid|double|groove|ridge|inset|outset|initial|inherit;
    *Estas medidas dependen del navegador*
    thick (ancho)
    medium (medio)
    thin (fino)

outline-color
    css native|rgb|rgba|hsl|hsla|hexadecimal|transparent
outline-top-(width | style | color)
outline-right-(width | style | color)
outline-bottom-(width | style | color)
outline-left-(width | style | color)
*/

[class*=box] {
    width: 200px;
    height: 200px;
    margin: 2em;
}

.box1 {
    background: yellowgreen;
    border: 5px solid red;
    border-top-width: 200px;
    border-top-color: transparent;
}
```
