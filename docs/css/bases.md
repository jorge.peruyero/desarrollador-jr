# Bases de CSS

## Como insertar CSS en HTML

Existen cuatro formas de incluit CSS en un documento HTML

- En la cabecera: a través de la etiqueta

    ``` HTML
    <style> código css  </style>
    ```

- En línea dentro de la etiqueta

    ``` HTML
    <p style="codigo css"></p>
    ```

- Asociado a una hoja de estilos externa a nuestro documento

    ``` HTML
    <link rel="stylesheet" href="styles.css">
    ```

- A través de @import detro de las etiquetas `<style></style>`

    ``` HTML
    <style>
      @import url("style.css");
    </style>
    ```

## Sintaxis de CSS

``` CSS
selector {
  propiedad: valor;
  -----------------
}
```

``` CSS
body {
  background: red;
  -----------------
}
```

### Ejemplo Como insertar CSS en HTML

``` HTML
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Mi primera web</title>
  <!-- Insertar CSS en la cabecera, no recomendable salvo que tengamos una sóla página o para pruebas 
    <style>
    p{
      color: red;
    }
  </style> 
  Estilo en línea, no recomendado para estilos estáticos, sólo para estilos que se tienen que calcular en tiempo real, con JavaScript por ejemlo.
   <h1 style="color:blue">Soy un encabezado principal</h1>
   Trayendo una hoja de estilos externa, la forma más recomendada para poder reutlizar el código
   <link rel="stylesheet" href="styles-dist.css">
   Importando con la regla @import url(), esta forma no se recomienda para evitar posibles fallos. 
   <style>
  @import url("styles-dist.css");
</style>
-->
<link rel="stylesheet" href="styles-dist.css">
</head>
<body>
  <h1>Soy un encabezado principal</h1>
  <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit autem vitae adipisci deserunt repellat. Sit sunt autem, possimus, quas quis architecto molestiae est eligendi quo libero quod eius ratione inventore. Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo error odio atque maxime consectetur odit ab facilis iure, mollitia veritatis fuga ullam exercitationem, vitae at aliquid officiis reprehenderit</p>

  <h1 class="title">Soy un encabezado principal</h1>
  <p class="text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit autem vitae adipisci deserunt repellat. Sit sunt autem, possimus, quas quis architecto molestiae est eligendi quo libero quod eius ratione inventore.</p>

  <h1 id="title">Soy un encabezado principal</h1>
  <p id="text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit autem vitae adipisci deserunt repellat. Sit sunt autem, possimus, quas quis architecto molestiae est eligendi quo libero quod eius ratione inventore. Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo error odio atque maxime consectetur odit ab facilis iure, mollitia veritatis fuga ullam exercitationem, vitae at aliquid officiis reprehenderit velit aliquam! Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste aliquid ratione, hic animi magni dolorem cupiditate explicabo, amet nisi corporis, molestiae architecto impedit ex optio minima iusto voluptatibus quae sunt?</p>

  <a class="rojo verde" href="">Enlace 1 rojo verde</a>
  <a class="rojo" href="#">Enlace 2 rojo</a>
  <a class="verde" href="#">Enlace 3 verde</a>
  <a class="verde rojo" href="#">Enlace 4 verde rojo</a>


  <h1>Título</h1>
  <h2>Subtítulo</h2>
  <h2>Otro subtítulo</h2>
  <h3>Otro subtítulo más</h3>
  <h3>Otro subtítulo más más</h3>

  <div>
    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Neque magni quidem recusandae non reiciendis ipsum earum, nam, <span>SOY UN SPAN</span> tenetur molestias, quia perferendis eligendi ratione eum at cumque facilis harum! Veritatis, quia.</p>
    <span>OTRO SPAN</span>
  </div>
</body>
</html>
```

``` CSS
/* Selector de etiqueta */
    /* p{
        color: red
    }
    h1{
        color: blue;
    } */

/* Selectores de clase */
    /* .title{
        color: green;
    }
    .text{
        color: blueviolet
    } */

/* Selectores de ID */
    /* #title{
        color: aqua;
    }
    #text{
        color: blueviolet;
    } */

/* Selectores universales */

    /* *{
        color: red;
    } */

/* Selectores de atributo */

/* Seleccionar los elementos que tienen ese atributo */
[href]{
    color: green;
}

/* Seleccionar los elementos que tienen ese atributo y ese valor*/
[href="#"]{
    color: red;
}

/* Seleccionar los elementos que tienen ese atributo al menos ese valor una vez*/
[class~="verde"]{
    color: green;
}

/* Seleccionar los elementos que tienen ese atributo y exactamente ese valor o empieze por el valor seguido de un guión*/
[class|="verde"]{
    color: blue;
}

/* Seleccionar los elementos que tienen ese atributo y empiezan por ese valor*/
[class^="rojo"]{
    color:purple;
}

/* Seleccionar los elementos que tienen ese atributo y terminan por ese valor*/
[class$="rojo"]{
    color:steelblue;
}

/* Seleccionar los elementos que tienen ese atributo y contengan ese valor*/
[class*="rojo"]{
    color:brown;
}

/* Selectores combinadores */

/* Selector de hermano adyacente */

/* Selecciona al hermano que esté justo por debajo de el. El estilo se aplica al hermano inferior */
h1 + h2{
    color:red;
}

h2 + h2{
    color: blue;
}

/* Selector de hermano general */

/* Selecciona todos los hermanos que esté por debajo de el. El estilo se aplica a todos los hermanos */
h1 ~ h3{
    color: green;
}

/* Selectores descendentes */

/* Selecciona todos los hijos. El estilo se aplica a todos los hijos */
div span{
    color: red;
}

/* Selector de hijo directo */

/* Selecciona todos los hijos que sean directos, es decir que no estén dentro de otras etiquetas. El estilo se aplica a todos los hijos */
p > span{
    color: blue;
}
```

## Especificidad, cascada y herencia

La herencia se aplica con el valor inherit, esto obliga al elemento a heredar la propiedad de su elemento más cercano

La cascada es el orden en el que se le aplicarán los estilos al ir leyendo el css. Los estilos que vienen después sobrescriben a los anteriores

La especificidad es el peso que tiene un selector cuando hay conflicto de estilos

- La w3c divide la especificidad en:

    | Etiqueta              | 0,0,0,1     |
    |-----------------------|-------------|
    | Clases y pseudoclases | 0,0,1,0     |
    | ID                    | 0,1,0,0     |
    | Estilos en línea      | 1,0,0,0     |
    | !important            | Gana a todo |

### Ejemplo Especificidad, cascada y herencia

``` HTML
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Especificidad, herencia y cascada</title>
  <link rel="stylesheet" href="styles-dist.css">
</head>
<body>

<p>Estamos viendo la <a href="#">herencia</a> en CSS</p>

<p class="parrafo">Estamos viendo la <a href="#">cascada</a> en CSS</p>

<p id="parrafo">Estamos viendo la <a href="#">cascada</a> en CSS</p>

<p class="parrafo-2">El párrafo que queremos cambiar y no podremos</p>
</body>
</html>
```

``` CSS
/*  Etiqueta -> 1
    Clases y pseudoclases -> 10
    Id -> 100
    Estilos en línea -> 1000
    !important -> Gana a todo;
    important -> especificidad -> cascada
*/

#parrafo{
    color: blueviolet;
}

.parrafo{
    color: red;
}

a{
    color: inherit;
}

.parrafo-2{
    color: black;
}

p{
    color: green;
}
```

## Resetear estilos por defecto

Por defecto cada navegador asigna estilos CSS a las páginas web que son usados hasta que se reemplazan en la hoja de estilos por lo que si queremos que nuestro sitio web se vea igual en cualquier navegador, es necesario normalizarlos.

Uno de estos normalizadores es [Normalize.css](https://necolas.github.io/normalize.css/) ya que hace que los navegadores representen todos los elementos de manera más consistente y en línea con los estándares modernos. Precisamente se dirige solo a los estilos que necesitan normalizarse.

### Ejemplo Resetear estilos por defecto

![banner](/desarrollador-jr/assets/img/css/bases/banner.png)

``` HTML
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Reset CSS</title>
   <link rel="stylesheet" href="normalize.css">
  <link rel="stylesheet" href="styles-dist.css">
</head>
<body>

<h1>Resetear Los Estilos CSS</h1>

<header>
    <div class="banner">
        <img src="banner.png" alt="Banner de la web">
    </div>
</header>

<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus cupiditate dignissimos ex, explicabo fugiat harum ipsum iste iusto natus officia placeat porro rerum tempora vero voluptatem? Asperiores at ex soluta.</p>

<div class="square"></div>

</body>
</html>
```

## BEM Organiza mejor tu CSS

``` HTML
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Reset CSS</title>
   <link rel="stylesheet" href="normalize.css">
  <link rel="stylesheet" href="styles-dist.css">
</head>
<body>

  <h1 class="main-title">Metodología BEM</h1>

  <header class="header">
    <nav>
        <ul class="main-menu">
            <li class="main-menu__item">
                <a class="main-menu__link" href="#">Item 1</a>
              </li>
            <li class="main-menu__item"><a class="main-menu__link main-menu__link--active" href="#">Item 2</a></li>
            <li class="main-menu__item"><a class="main-menu__link" href="#">Item 3</a></li>
            <li class="main-menu__item"><a class="main-menu__link" href="#">Item 4</a></li>
        </ul>
    </nav>
  </header>

<main class="main"></main>

<footer class="footer">

</footer>

</body>
</html>
```

``` CSS
/*
Los bloques se suelen denominar con una sola palabra o con dos palabras separadas con un guion si puede existir conflicto de estilos.
.menu / .main-menu
.gallery / .main-gallery
Los elementos se nombran con el nombre del bloque al que pertenecen y su nombre con dos guiones bajos __
.menu__item / .main-menu__link
.gallery__img  / .main-gallery__item
Los modificadores se nombran con el nombre del bloque al que pertenecen y su modificador con dos guiones medios --
.menu__item--active
.gallery__item--special
Otras metodologías de nomenclatura CSS:
SuitCSS
BEMIT
*/

.main-title{text-align: center;}

.header{background: lightblue;height: 50px;}

.main{background:lightseagreen;height: 600px;}

.footer{background: lightsalmon;height: 200px;}

/* nav ul {
    margin-top: 0;
    margin-bottom: 0;
    padding-left: 0;
    list-style: none;
    display: flex;
    justify-content: space-evenly;
}
nav ul li{
    line-height: 3;
}
nav ul li a{
    text-decoration: none;
    font-size: 1.2em;
    color: green
}
a{
    color: red
} */

.main-menu{
    margin-top: 0;
    margin-bottom: 0;
    padding-left: 0;
    list-style: none;
    display: flex;
    justify-content: space-evenly;
}

.main-menu__item{
    line-height: 3;
}

.main-menu__link{
    text-decoration: none;
    font-size: 1.2em;
    color: green;
}

.main-menu__link--active{
    background: red;
}

/* Forma correcta según la metodología BEM */

.main-form{

}

.main-form__label{

}

.main-form__submit{

}

/* Esta forma no es recomendada */
.form{

}

.label{

}

.submit{

}
```
