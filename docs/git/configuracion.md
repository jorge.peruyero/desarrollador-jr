# Configuración de un repositorio

## Ajustes y configuración

Git almacena las opciones de configuración en tres archivos distintos, lo que te permite ajustar opciones para repositorios individuales (local), usuarios (global) o todo el sistema (sistema):

- Local: /.git/config —ajustes específicos del repositorio.
- Global: ~/.gitconfig —ajustes específicos del usuario. Aquí es donde se almacenan las opciones configuradas con la marca --global.
- Sistema: $(prefix)/etc/gitconfig —ajustes de todo el sistema.

Define el nombre del autor que se va a usar en todas las confirmaciones del repositorio actual. Normalmente, será preferible utilizar el indicador `--global` para establecer las opciones de configuración del usuario actual.

``` sh
git config --global user.name
```

Define el nombre del autor que se va a usar en todas las confirmaciones del usuario actual.

``` sh
git config --global user.email
```

Define el correo electrónico del autor que se va a usar en todas las confirmaciones del usuario actual.

## Inicio de un nuevo repositorio

Para crear un nuevo repositorio, usa el comando `git init`.

`git init` es un comando que se utiliza una sola vez durante la configuración inicial de un repositorio nuevo. Al ejecutar este comando, se creará un nuevo subdirectorio .git en tu directorio de trabajo actual. También se creará una nueva rama maestra.

``` sh
git init
```

## Guardar cambios en el repositorio

Ahora que has iniciado un repositorio, puedes realizar commits en la versión del archivo.

Primeramente hay que tener algún cambio para esto usa el comando `git status`

``` sh
git status
```

Con esto veremos un listado de archivos que se encuentran alojados en working directoriy, para agregar estos archivos al staging area usa el comando `git add`

``` sh
git add
```

Aquí se puede seleccionar uno a uno los archivos que se enviarán al staging area, si se quire enviar todos los archivos disponibles usar `git add -A`

``` sh
git add -A
```

Una vez teniendo los archivos en staging area ya se puede realizar el commit correspondiente a la funcionalidad que se está desarrollando, usa el comando `git commit`

``` sh
git commit
```

Para agregar un comentario relacionado al commit que se está creando y usa el comando `git commit -m 'comentario'`

``` sh
git commit -m 'comentario'
```

En caso de haberse equivocado durante el último commit y desea corregir el commit agregando un nuevo archivo usa el comando `git commit --amend`

``` sh
git add <nombre de nuevo archivo>
git commit --amend
```

Con esto se agrega el nuevo archivo a al staging area y se actualiza el último commit manteniendo el último mensaje.
