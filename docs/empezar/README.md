---
sidebarDepth: 2
---
# Creación de proyectos Digiceo

## Objetivos de aprendizaje

Que el desarrollador pueda iniciar un proyecto desde cero haciendo uso de HTML, CSS y PHP conectándolo a un backend proporcionado por Digiceo como también dar mantenimiento a los sitios ya creados como Cybertool, Instituto K29 entre otros.

## Misión

Enseñar al alumno los requisitos técnicos de la creación de un proyecto desde cero y sus conexiones a la base de datos en menos de un mes

## Visión

Qué el alumno sea capaz de dar mantenimiento y crear nuevas características a los diferentes proyectos

## Material requerido

1. Computadora con conexión a internet
2. [Gitfiend](https://gitfiend.com/)
3. [Visual Studio Code](https://code.visualstudio.com/download)
4. Servidor
    - [Xampp](https://www.apachefriends.org/es/download.html) o [Mamp](https://www.mamp.info/en/windows/) Server
5. [Filezilla](https://filezilla-project.org/download.php)
6. [Git](https://git-scm.com/)

## Temas a tratar

1. Instalación y configuración de los programas a utilizar
2. [Introducción a Git](/git/) (consola y cliente gráfico)
    - Temario

        Introducción a Git

        Iniciar un repositorio

        Hacer Pull request

        Hacer Push

        Crear un Branch

        Enviar cambios de branch a master

        Clonar un repositorio

3. [Introducción a HTML](/html/)
    - Temario
        - Sección 1: Introducción
            1. Conocimientos básicos y requisitos técnicos
            2. ¿Qué es HTML?
            3. Estructura de las etiquetas HTML
        - Sección 2: Etiquetas básicas
            1. Etiquetas básicas parte: 1
            2. Etiquetas básicas parte: 2
            3. Etiquetas básicas parte: 3
        - Sección 3: Estilos, semántica y navegación
            1. Estilos, semántica y navegación parte 1
            2. Estilos, semántica y navegación parte 2
            3. Estilos, semántica y navegación parte 3
        - Sección 4: Multimedia
            1. Multimedia
        - Sección 5: Formularios
            1. Formularios parte 1
            2. Formularios parte 2
        - Sección 6: Metadata

            Metadata

4. [Introducción a CSS](/css/)
    - Temario
        - Sección 1: Introducción
        - Sección 2: Bases de CSS
        - Sección 3: Box Model
        - Sección 4: Position
        - Sección 5: Display, pseudoelementos y pseudoclases
        - Sección 6: Background
        - Sección 7: Textos y tipografías
        - Sección 8: Listas y tablas
        - Sección 9: Imágenes
        - Sección 10: Colores, border-radius,
        - Sección 11: Overflow y float
        - Sección 12: Flexbox
        - Sección 13: Grid
        - Sección 14: Responsive
5. Creación de un sitio básico combinando HTML y CSS con las secciones que tiene digiceo
    1. Utilización de bootstrap para acelerar el proceso
    2. Creación de cada una de las secciones de un sitio similar a plantilla Digiceo
    - Temario
        - Sección 1: Bootstrap básico
        - Sección 2: Creación de un navbar
        - Sección 3: Creación de header
        - Sección 4: Creación de sección descripción
        - Sección 5: Creación de sección galería
        - Sección 6: Creación de sección facilidades
        - Sección 7: Creación de sección servicios
        - Sección 8: Creación de sección contacto
6. Backend Digiceo
    1. Descargar Backend
    2. Realizar conexiones a base de datos
    - Temario
        - Sección 1: Creación de una base de datos
        - Sección 2: Descargar fragmento funcional de backend de Digiceo
        - Sección 3: Buscar las variables a reemplazar por los datos de la nueva base de datos
7. Conexión de backend con frontend
    1. Conexión sitio básico previamente creado con backend
    - Temario
        - Sección 1: Seccionar y componentizar las secciones de HTML a PHP
        - Sección 2: Agregar conexión a backend
        - Sección 3: Hacer conexiones de cada sección a backend
        - Sección 4: Subir el sitio a producción con FileZilla
