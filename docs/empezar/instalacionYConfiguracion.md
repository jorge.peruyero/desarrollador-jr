# Instalación y configuración de software

## [Visual Studio Code](https://code.visualstudio.com/download)

  Visual Studio Code es un editor de código fuente desarrollado por Microsoft para Windows, Linux y macOS. Incluye soporte para la depuración, control integrado de Git, resaltado de sintaxis, finalización inteligente de código, fragmentos y refactorización de código. También es personalizable, por lo que los usuarios pueden cambiar el tema del editor, los atajos de teclado y las preferencias. Es gratuito y de código abierto, aunque la descarga oficial está bajo software privativo e incluye características personalizadas por Microsoft.

### Extensiones

- [Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)

    Esta extensión permite identificar los corchetes a juego con los colores. El usuario puede definir qué tokens combinar y qué colores usar.

- [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)

    Lanzar un servidor local de desarrollo con función de recarga en vivo para páginas estáticas y dinámicas.

- [HTML CSS Support](https://marketplace.visualstudio.com/items?itemName=ecmel.vscode-html-css)

    Soporte CSS para documentos HTML.

- [Auto Close Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag)

    Agregue automáticamente la etiqueta de cierre HTML / XML, igual que Visual Studio IDE o Sublime Text

- [GitLens — Git supercharged](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)

    Mejore las capacidades de Git integradas en Visual Studio Code: visualice la autoría del código de un vistazo a través de anotaciones de culpa de Git y lentes de código, navegue y explore sin problemas los repositorios de Git, obtenga información valiosa a través de poderosos comandos de comparación y mucho más.

- [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph)

    Vea un gráfico de Git de su repositorio y realice fácilmente acciones de Git desde el gráfico. ¡Configurable para lucir como quieras!.

- [Color Picker](https://marketplace.visualstudio.com/items?itemName=anseki.vscode-color)

    Ayuda con GUI para generar códigos de color como notaciones de color CSS.

## [Git](https://git-scm.com/)

  Git es un sistema de control de versiones distribuido gratuito y de código abierto diseñado para manejar todo, desde proyectos pequeños a muy grandes, con velocidad y eficiencia.

## [Gitfiend](https://gitfiend.com/)

  Un cliente de Git diseñado para humanos

## Servidor

- [Xampp server](https://www.apachefriends.org/es/download.html)

    XAMPP es una distribución de Apache fácil de instalar que contiene MariaDB, PHP y Perl.

- [Mamp server](https://www.mamp.info/en/windows/)

    MAMP es un entorno de servidor local gratuito que se puede instalar en macOS y Windows con solo unos pocos clics.

## [Filezilla](https://filezilla-project.org/download.php)

  FileZilla es una aplicación FTP libre y de código abierto que consta de un cliente y un servidor. Soporta los protocolos FTP, SFTP y FTP sobre SSL/TLS.
